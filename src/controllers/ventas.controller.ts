import { Request, Response } from "express";
import IVentas from "../interfaces/ventas.inteface";
import Ventas from "../models/ventas.model";

// Get all resources
export const index = async (req: Request, res: Response) => {
    // agregar filtros

    try {
        let ventas = await Ventas.find().populate({ path: 'persona_id', select: ['id', 'nombreCompleto', 'email'] })
        .populate({ path: 'productos.producto_id', select: ['id', 'nombre', 'stock'] }); 

        res.json(ventas);
    } catch (error) {
        res.status(500).send('Algo salió mal');
    }
};

// Get one resource
export const show = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let venta = await Ventas.findById(id);

        if (!venta)
            res.status(404).send(`No se encontró la venta con id: ${id}`);
        else
            res.json(venta);
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

// Create a new resource
export const store = async (req: Request, res: Response) => {
    try {
        const { ...data } = req.body;
        //res.status(200).send(data.productos);

        const venta: IVentas = new Ventas({
            forma_de_pago: data.forma_de_pago,
            precio_total: data.precio_total,
            estado : data.estado,
            persona_id: data.persona_id,
            productos :data.productos
        });

        await venta.save();

        res.status(200).json(venta);
    } catch (error) {
        console.log(error);
        res.status(500).send('Algo salió mal.');
    }
};

// Edit a resource
export const update = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    const { ...data } = req.body;
    try {
        let venta = await Ventas.findById(id);

        if (!venta)
            return res.status(404).send(`No se encontró la venta con id: ${id}`);
        
        if (data.forma_de_pago) venta.forma_de_pago = data.forma_de_pago;
        if (data.precio_total) venta.precio_total = data.precio_total;
        if (data.estado) venta.estado = data.estado;
        if (data.productos) venta.productos = data.productos;
        await venta.save();
        
        res.status(200).json(venta);
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

// Delete a resource
export const destroy = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let venta = await Ventas.findByIdAndDelete(id);

        if (!venta)
            res.status(404).send(`No se encontró la venta con el id: ${id}`);
        else
            res.status(200).json(venta);
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};