import { model, Schema } from 'mongoose';
import Iventas from '../interfaces/ventas.inteface';

const VentasSchema = new Schema({
    forma_de_pago: {
        type: String,
        required: [true, 'El nombre es obligatorio'],
        enum: ['contado', 'tarjeta']
    },
    precio_total: {
        type: Number,
        required: [true, 'El precio es obligatorio']
    },
    estado: {
        type: String,
        required: [true, 'El estado es obligatorio. Valores posibles: APROBADA/ANULADA'],
        enum: ['APROBADA', 'ANULADA']
    },
    persona_id: {
        type: Schema.Types.ObjectId,
        ref: 'Personas',
        required: true
      },
      productos: {
        type: Schema.Types.Array,
      }
}, {
    timestamps: { createdAt: true, updatedAt: true }
})

export default model<Iventas>('ventas', VentasSchema);