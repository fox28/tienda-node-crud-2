import { model, Schema } from 'mongoose';
import IPersona from '../interfaces/persona.interface';

const PersonaSchema = new Schema({
    nombreCompleto: {
        type: String,
        required: [true, 'El nombre completo es obligatorio']
    },
    email: {
        type: String,
        required: [true, 'El email es obligatorio y único'],
    },
    contraseña: {
        type: String,
        required: [true, 'La contraseña es obligatoria']
    },
    telefono: {
        type: String,
        required: [true, 'El telefono es obligatorio']
    },
    rol: {
        type: String,
        required: [true, 'El rol es obligatorio. Valores posibles: admin/cliente'],
        enum: ['admin', 'cliente']
    }
}, {
    timestamps: { createdAt: true, updatedAt: true }
})

export default model<IPersona>('Personas', PersonaSchema);